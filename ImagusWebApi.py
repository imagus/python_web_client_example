﻿import os
import errno
import urllib2
import urllib
import json
import requests
import logging




# These two lines enable debugging at httplib level (requests->urllib3->http.client)
# You will see the REQUEST, including HEADERS and DATA, and RESPONSE with HEADERS but without DATA.
# The only thing missing will be the response.body which is not logged.
##
#try:
#    import http.client as http_client
#except ImportError:
#    # Python 2
#    import httplib as http_client
#http_client.HTTPConnection.debuglevel = 1
#
## You must initialize logging, otherwise you'll not see debug output.
#logging.basicConfig() 
#logging.getLogger().setLevel(logging.DEBUG)
#requests_log = logging.getLogger("requests.packages.urllib3")
#requests_log.setLevel(logging.DEBUG)
#requests_log.propagate = True
##









class Point(object):
    x = None
    y = None

    def __init__(self, dict):
        self.x = float(dict['x'])
        self.y = float(dict['y'])

    def __str__(self):
        return "%6.1f, %6.1f" % (self.x, self.y)

    def __eq__(self, obj):
        return obj.x == self.x and obj.y == self.y

class PointPair(object):
    left = None
    right = None
    def __init__(self,dict):
        self.left = Point(dict['left'])
        self.right = Point(dict['right'])

    def __str__(self):
        return "%s,%s" % (str(self.left), str(self.right))

    
class Face(object):

    eyes = None
    mouth = None

    def __init__(self, dict):
        self.eyes = PointPair(dict['eyes'])
        self.mouth = PointPair(dict['mouth'])
        self.quality = float(dict['quality'])
        self.centre = Point(dict['centre'])
        self.angle = float(dict['angle'])
        self.size = float(dict['size'])

    def __repr__(self):
        return json.dumps(self.__dict__)

class ImagusJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        
        if isinstance(obj, Point):
            return obj.__dict__
        if isinstance(obj, PointPair):
            return obj.__dict__
        if isinstance(obj, Face):
            return obj.__dict__
        else:
            return json.JSONEncoder.default(self, obj)


class Imagus(object):
    def __init__(self, serverurl, username, password):
        self.serverurl = serverurl

        response = json.load( urllib2.urlopen(self.serverurl + "/api/v1/logins") )
        data = response['logins']

        for login in data:
            if login['type'] == 'api' and login['name'] == 'imagus':
                auth_url = login['auth_url']
        if auth_url is None:
            raise Exception("cannot get auth url")


        auth_url += '&' + urllib.urlencode({'username':username,'password':password})#=%s&password=%s' % (auth_url, username, password)
        response = urllib2.urlopen(auth_url)
        # get the user's cookie
        cookie = response.info()['Set-Cookie']
        self.headers = {'Cookie': cookie}


    def __build_request(self, url, params=None, method='GET'):
        theurl = self.serverurl + url
        if params != None:
            theurl += '?' + urllib.urlencode(params)
        
        
        requests_header = {'Cookie': self.cookie}
        if method == 'POST' :
            resp = requests.post(theurl, None, headers=requests_header, verify=False)
        else :
            resp = requests.get(theurl, headers=requests_header, verify=False)
        return resp.text
        
        
    def __get(self, url, **kwargs):
        response = requests.get(self.serverurl + url,headers=self.headers,**kwargs)    
        response.raise_for_status()
        return response

    def __post(self, url, data, **kwargs):
        response = requests.post(self.serverurl + url,data, headers=self.headers,**kwargs)
        response.raise_for_status()
        return response



    def GetUser(self):
        return self.__get("/api/v1/user").json()

    def create_person(self, person_name):
        return self.__post("/api/v1/person/new",None,params={'name':str(person_name)}).json()['person_uid']

    def tag_person(self,person_id, tag):
        response = self.__post("/api/v1/person/"+person_id+"/tag",None,params={'tag':tag })
        return response.json()['tags']
        

    def find_people(self, person_name,count=30):
        return self.__get("/api/v1/person",params={'q':str(person_name),'rate':str(count)}).json()['people']

    def upload_file(self, filename):
        with open(filename,'rb') as file:
            response = self.__post("/api/v1/image/upload",params={'file_name':filename},data=file)
            json = response.json()
            return json['token']

    def detect_faces(self, image_token):
        response = self.__get("/api/v1/image/detect",params={'token':image_token})
        faces = []
        for v in response.json()['faces']:
            faces.append(Face(v))
        
        return faces

    def detect_largest_face(self, image_token):
        face = None
        for f in self.detect_faces(image_token):
            if face == None or face.size < f.size:
                face = f
        return face


    def detect_identity_face(self, image_token):
        response = self.__get("/api/v1/image/detect/identity",params={'token':image_token})
        for v in response.json()['faces']:
            return Face(v)
        return None

    def enrol_face(self,image_token,face,person_id):
        response = self.__post("/api/v1/person/"+person_id+"/enrol",None,params={'token':image_token,'eyes':json.dumps(face.eyes,cls=ImagusJsonEncoder)})
        return response.json()['enrolment_uid']
    
    def enrol_photo(self, person_id, photofile):

        enrolment_token = None

        imagus_token = self.upload_file(photofile)
        face = self.detect_largest_face(imagus_token)

        if face is not None:
            enrolment_token = self.enrol_face(imagus_token, face, person_id)

        return enrolment_token

    def search_photo(self, photofile):

        token = self.upload_file(photofile)
        face  = self.detect_largest_face(token)
        if (face is None):
            return []

        response = self.__get("/api/v1/face/search", params={'token':token, 'eyes':json.dumps(face.eyes,cls=ImagusJsonEncoder), "model": "high", "unique": "true"})
        j = response.json()
        return j['search_results']


